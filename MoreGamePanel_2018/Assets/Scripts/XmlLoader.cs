﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;

public class XmlLoader : MonoBehaviour
{
    public Transform MoreGamePanel;
    public Transform MoreGameList;
    public Transform MoreGameObject;
    public Button MoreGameButton;
    public Button BackButton;
    List<RectTransform> Games;
    TextAsset xmlRawFile;
    public string moreGamesUrl;

    int nodeCount;
    int currentNode = 0;
    int numberOfIconLoaded;
    int numberOfFeatureImagesLoaded;
    bool allImagesLoaded;

    void Start()
    {
        allImagesLoaded = false;
        Games = new List<RectTransform>();
        //string xmlFileData = xmlRawFile.text;
        parseXml();
    }

    void parseXml()
    {
        
        XmlDocument xmlFile = new XmlDocument();
        xmlFile.Load("https://drive.google.com/u/0/uc?id=1bLjWDbSnAbQhRJ-5b0tFSG28NsYsSXmt&export=download");
       
        string xmlPathPattern = "//root/game";
        XmlNodeList nodes = xmlFile.SelectNodes(xmlPathPattern);
        nodeCount = nodes.Count;
        print("Total Games " + nodeCount);

        foreach (XmlNode node in nodes)
        {
            Games.Add(Instantiate(MoreGameObject, MoreGameList) as RectTransform);

            XmlNode name = node.FirstChild;
            XmlNode rating = name.NextSibling;
            XmlNode icon = rating.NextSibling;
            XmlNode featureGraphic = icon.NextSibling;
            XmlNode gameLink = featureGraphic.NextSibling;

            Games[currentNode].GetComponent<GameAttributes>().GameName.text = name.InnerText;
            Games[currentNode].GetComponent<GameAttributes>().GameLink = gameLink.InnerText;

            Games[currentNode].GetComponent<GameAttributes>().showStars(rating.InnerText);


            StartCoroutine(LoadIconFromLink(icon.InnerText, currentNode));
            StartCoroutine(LoadFeatureImageFromLink(featureGraphic.InnerText, currentNode));

            currentNode++;

        }
    }
    IEnumerator LoadIconFromLink(string iconUrl, int count)
    {
        UnityWebRequest iconLoader = UnityWebRequestTexture.GetTexture(iconUrl);
        yield return iconLoader.SendWebRequest();
        Games[count].GetComponent<GameAttributes>().Icon.texture = DownloadHandlerTexture.GetContent(iconLoader);
        numberOfIconLoaded++;
        print("Total Icons loaded " + numberOfIconLoaded);

        if (numberOfIconLoaded == nodeCount)
            CheckForAllElementsLoaded();
    }
    IEnumerator LoadFeatureImageFromLink(string featureImageUrl, int count)
    {
        UnityWebRequest featureImageLoader = UnityWebRequestTexture.GetTexture(featureImageUrl);
        yield return featureImageLoader.SendWebRequest();
        Games[count].GetComponent<GameAttributes>().featureGraphic.texture = DownloadHandlerTexture.GetContent(featureImageLoader);
        numberOfFeatureImagesLoaded++;
        print("Total FeatureImages loaded " + numberOfFeatureImagesLoaded);

        if (numberOfFeatureImagesLoaded == nodeCount)
            CheckForAllElementsLoaded();

    }
    public void CheckForAllElementsLoaded()
    {
        if ((numberOfFeatureImagesLoaded == nodeCount) && (numberOfIconLoaded == nodeCount))
        {
            allImagesLoaded = true;
            //MoreGameButton.GetComponent<Image>().color = Color.green;
        }
    }
    public void OnMoreButtonClick()
    {
        if (!allImagesLoaded)
            Application.OpenURL(moreGamesUrl);
        else
        {
            MoreGamePanel.gameObject.SetActive(true);
            MoreGameButton.gameObject.SetActive(false);
            BackButton.gameObject.SetActive(true);

        }
    }
}
    
