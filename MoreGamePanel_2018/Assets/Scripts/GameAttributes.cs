﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameAttributes : MonoBehaviour
{
    public Text GameName;
    public Image Stars;
    public RawImage Icon;
    public RawImage featureGraphic;
    public string GameLink;

    public void showStars(string count)
    {
        
        float rating = float.Parse(count);
        print(rating);
        Stars.fillAmount = rating*0.2f;
    }
    public void OnGameClick()
    {
        Application.OpenURL(GameLink);
    }
}
